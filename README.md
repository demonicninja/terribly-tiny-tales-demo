# Frontend
The project is made in Angular 5 using `angular-cli`.
Angular material library (https://material.angular.io)
and Bootstrap 4 (https://getbootstrap.com/)
is used for layout and templating the ui

For building and running the project, please refer to the README file inside frontend directory.

You can see the live demo hosted on firebase at:
[https://kushangpatel.com/terribly-tiny-tales/](https://kushangpatel.com/terribly-tiny-tales/)

# Backend
The project is created using nodejs, express and typescript.
Node's internally available `http module` is used to fetch and read the provided text file by Terribly Tiny Tales.

To run the application:
```
cd backend
yarn install // or npm install
npm start
```

Live server is hosted on Heroku:
[https://ttt-demo-kushang.herokuapp.com/](https://ttt-demo-kushang.herokuapp.com/?n=3)