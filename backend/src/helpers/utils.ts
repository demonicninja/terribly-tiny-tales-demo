export function isNull(str: string): boolean {
  if (undefined === str || null === str || '' === str) {
    return true
  }

  return false
}