import { Http } from '@angular/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Rx'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'

@Injectable()
export class ApiService {

  constructor(private http: Http) { }

  getTerribleResult(numVal: number) {
    return this.http.get('https://ttt-demo-kushang.herokuapp.com?n=' + numVal)
      .map(res => res.json())
      .catch(res => {
        return Observable.throw(res.json())
      })
  }

}
