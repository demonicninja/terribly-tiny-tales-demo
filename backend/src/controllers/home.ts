import * as http from 'http'
import { isNull } from '../helpers/utils'
import { Request, Response } from 'express'

/**
 * GET /
 * Home page.
 */
export let index = async (req: Request, res: Response) => {
  let topNWords: number = req.query.n
  let error: any
  let response: any[] = []
  let wordsCounter: number = 0

  if (undefined === topNWords || null === topNWords) {
    error = { message: 'Invalid input' }
  }

  try {
    let terriblyTinyTalesResp = await getResponse()
    // replace tabs by space, multiple spaces by single space, new line with single space and then split the string by space
    let responseArr = terriblyTinyTalesResp.replace(/\t/g, ' ').replace(/ +(?= )/g, '').replace(/\n/g, ' ').split(' ')
    // map to hold the words and their count
    let wordMap = {}
    for (let word of responseArr) {
      if (isNull(wordMap[word])) {
        wordMap[word] = 1
      } else {
        wordMap[word] = wordMap[word] + 1
      }
    }
    // sort the words in decreasing order of their occurences    
    let keysSorted = sortObject(wordMap, 'desc')

    for (let sorted of keysSorted) {
      if (!isNull(sorted) && !isNull(sorted.key) && wordsCounter < topNWords) {
        response.push(sorted)
        wordsCounter++
      }
    }

  } catch (err) {
    console.error(err)
    error = err
  }
  if (error) {
    res.send(error)
  } else {
    res.send(response)
  }
};

async function getResponse(): Promise<any> {
  return new Promise((resolve, reject) => {
    let response: any = ''
    http.get('http://terriblytinytales.com/test.txt', (response: any) => {
      response.on('data', (data: any) => {
        response += data
      })
      response.on('end', () => {
        resolve(response)
      })
      response.on('error', (error: any) => {
        reject(error)
      })
    })
  })

}

/**
 * 
 * @param obj javascript object
 * @param type string asc | desc
 */
function sortObject(obj: any, type: string): any[] {
  let arr = [];
  for (let prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      arr.push({
        'key': prop,
        'value': obj[prop]
      });
    }
  }
  arr.sort(function (a, b) {
    if (type === 'asc') {
      return a.value - b.value;
    } else if (type === 'desc') {
      return b.value - a.value
    }
  });
  //arr.sort(function(a, b) { a.value.toLowerCase().localeCompare(b.value.toLowerCase()); }); //use this to sort as strings
  return arr; // returns array
}