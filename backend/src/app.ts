import * as bodyParser from 'body-parser'
import * as cors from 'cors'
import * as express from 'express'
import * as homeController from './controllers/home'
import * as path from 'path'

// Create Express server
const app = express();

// Express configuration
app.set("port", process.env.PORT || 3000);
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/**
 * Primary app routes.
 */
app.get("/", homeController.index);

module.exports = app;