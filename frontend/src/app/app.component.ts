import { ApiService } from './services/api/api.service'
import {
  Component,
  ElementRef,
  OnInit,
  ViewChild
  } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { LoaderService } from './services/loader/loader.service'
import { MatSnackBar } from '@angular/material/snack-bar'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public numForm: FormGroup
  public numVal: number
  public apiResponse: any[] = []

  @ViewChild('numValTxt') numValTxt: ElementRef

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService,
    private loader: LoaderService,
    private snackBar: MatSnackBar
  ) {

  }

  ngOnInit() {
    // initialize form group and components for validation
    this.numForm = this.formBuilder.group({
      numVal: [null, [Validators.required]]
    })

    // default focus
    this.numValTxt.nativeElement.focus()
  }

  public onSubmit() {
    if (this.numForm.valid) {
      console.log('Form submitted: ', this.numVal)
      this.getTerribleResult()
    } else {
      this.snackBar.open('Please input a number')
    }
  }

  public getTerribleResult() {
    this.loader.display(true)
    this.apiService.getTerribleResult(this.numVal).subscribe((res) => {
      this.loader.display(false)
      this.apiResponse = res
      console.log('Api Response: ', this.apiResponse)
    }, (error) => {
      this.loader.display(false)
      this.snackBar.open('There was some error getting result')
    })
  }
}
